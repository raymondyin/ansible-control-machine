# Ansible Control Machine
This is a sample project that provides a VM with Ansible and any Ansible-Galaxy roles installed.
The intent of this is to give a baseline Control Machine definition as a starter that can be copied into your existing projects.

Why would I want an Ansible Control Machine VM? Isolation. A control machine let's you isolates project specific DevOps work into a VM. For example
your Host OS does not get cluttered with Ansible Galaxy roles from numerous projects, or you can keep project specific private keys in the control VM. 
You can also keep project specific installations and packages which an Ansible Control Machine might require in your VM, such as specific Python versions or packages.
Another reason is for compatability. Windows users will be able to run Ansible, as well ensuring any 'local_action' tasks work for all developers becomes simplified.

## Prerequisites
1. Virtualbox
1. Vagrant (latest. 1.8.1 at the time of writing)

## Suggestions for use in project.
Since the purpose is to use this project as a reference to include in your own, there are a few suggestions on how to put this in your project.

1. Create a 'devops' subfolder in your project and copy all of the files there.
1. Copy and paste the 'ansible-control-machine' definition from the Vagrantfile here into the Vagrantfile of your own project. Suggest putting autostart:false on that VM.
Copy over the other files to sit in the same folder as your Vagrantfile